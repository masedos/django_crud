# -*- encoding: utf-8 -*-

from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.contrib.auth import login, logout
from django.contrib.auth.decorators import login_required
from .forms import ContatoForm, LoginForm
from django.utils import timezone
from .models import Contato

# Create your views here.

def index(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            return HttpResponseRedirect(reverse('index'))
    else:
        form = LoginForm()
    return render(request, 'index.html', {
        'form': form,
        })


@login_required
def cadastro(request):
    if request.method == 'POST':
        form = ContatoForm(request.POST)
        if form.is_valid():
            novo_contato = form.save()
            return HttpResponseRedirect(reverse('listar'))
    else:
        form = ContatoForm()

    return render(request, 'core/cadastro.html', {
        'form': form,
        })


@login_required
def listar(request):
    contatos = Contato.objects.all()
    return render(request, 'core/listar.html', {'contatos': contatos})


@login_required
def update(request, id):
    contatos = get_object_or_404(Contato, id=id)
    if request.method == 'POST':
        form = ContatoForm(request.POST, instance=contatos)
        if form.is_valid():
            novo_contato = form.save()
            return HttpResponseRedirect(reverse('listar', id=contatos.id))
    else:
        form = ContatoForm(instance=contatos)
    return render(request, 'core/update.html', {'contatos': contatos, 'form': form})

@login_required
def excluir(request, id):
    contato = get_object_or_404(Contato, id=id)
    contato.delete()
    return HttpResponseRedirect(reverse('listar'))


@login_required
def detail(request, id):
    contato = get_object_or_404(Contato, id=id)
    return render(request, 'core/detail.html', {'contato': contato})


@login_required
def sair(request):
    logout(request)
    return HttpResponseRedirect(reverse('index'))
    
