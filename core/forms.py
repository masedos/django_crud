# -*- encoding: utf-8 -*-

from django import forms
from core.models import Contato
from django.contrib.auth.models import User
from django.contrib.auth import authenticate

# Create your form here.

class ContatoForm(forms.ModelForm):
    nome = forms.CharField(max_length=30)
    sobrenome = forms.CharField(max_length=30)
    email = forms.EmailField()
    telefone = forms.CharField(max_length=20)
    observacao = forms.Textarea()

    class Meta:
        model = Contato
        fields = ['nome', 'sobrenome', 'email', 'telefone', 'observacao']

class LoginForm(forms.Form):
    username = forms.CharField(
        label='Usuario',
        max_length=30,
    )

    password = forms.CharField(
        label='Senha',
        max_length=30,
        widget=forms.PasswordInput
    )

    def clean_username(self):
        username = self.cleaned_data.get('username')
        if not User.objects.filter(username=username):
            raise forms.ValidationError('Usuario nao existe.')
        return username

    def clean_password(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')
        if not authenticate(username=username, password=password):
            raise forms.ValidationError('Senha incorreta.')
        return password

    def save(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')
        return authenticate(username=username, password=password)