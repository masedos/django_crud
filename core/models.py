# -*- encoding: utf-8 -*-

from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone

# Create your models here.

class Contato(models.Model):
    nome = models.CharField(max_length=30)
    sobrenome = models.CharField(max_length=30)
    email = models.EmailField()
    telefone = models.CharField(max_length=20)
    observacao = models.CharField(max_length=400)
    created_date = models.DateTimeField(default=timezone.now)
    published_date = models.DateTimeField(blank=True, null=True)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.nome


class PerfilUsuario(models.Model):
    user = models.OneToOneField(
        User,
        related_name='perfil_usuario',
        )

    def __str__(self):
        return self.user.username
