## Django CRUD


## Crud: https://django-crud-masedos.c9users.io/


1) Initialized empty Git repository in ~/django_crud/.git/

    $ git init  
    $ git config user.name "Fernandes Macedo"
    $ git config user.email masedos@egmail.com
    $ git status
    $ git add -A .
    $ git commit -m "first commit"

2) Create a new repository on github django_crud

    $ git remote add origin https://masedos@bitbucket.org/masedos/django_crud.git
    $ git push -u origin master


## Starting from the Terminal

In case you want to run your Django application from the terminal just run:

3) Run migrate command to sync models to database and create Django's default superuser and auth system

    $ python manage.py makemigrations
    $ python manage.py migrate

4) Run Django

    $ python manage.py runserver $IP:$PORT
    
5) Link
    sudo ln -nsf /usr/bin/python3.5  /usr/bin/python 
    